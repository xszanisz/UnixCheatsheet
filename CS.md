**tcpdump with cyclic pcap save buffers**

    tcpdump -i vlan2 icmp6 -G 86400 -w /tmp/capture-%w.pcap

| -G    | -w | description       |
| ----- | -- | ----------------- |
| 86400 | %w | 7 saves daily     |
| 3600  | %H | 24 saves hourly   |
| 60    | %M | 60 saves minutely |

- - -

**Timestamping**

    command | awk '{ print strftime("[%F %T]"), $0 }'

Source: <https://unix.stackexchange.com/questions/26728/prepending-a-timestamp-to-each-line-of-output-from-a-command>

- - -

**Order mutt mails by date**

    :set sort=date-received

- - -

**Change keyboard layout in console (Debian)**

    dpkg-reconfigure keyboard-configuration
    # apply changes (may not be necessary)
    systemctl restart keyboard-setup

Source: <https://wiki.debian.org/Keyboard>

- - -

**Check/Open SSL connection (command line)**

    echo | openssl s_client -connect $HOST -port $PORT -servername $SNI

- - -

**Resize an ext partition in LVM**

    lvresize -r -L+20G vg/lv

Or more manually

    lvresize -L+20G vg/lv
    resize2fs /dev/mapper/vg-lv

- - -

**Useful `grep` options**

    -l  --files-with-matches
    -o  --only-matching
    -H  --with-filename

- - -

**Handling NUL-separated strings**

* `grep`: `-z` for input, `-Z` for output
* `find`: `-print0` for output
* `sort`: `-z` for both input and output
* `awk`: `BEGIN { RS="\0" }` for input, `BEGIN { ORS="\0" }` for output
* `xargs`: `-0` for input
* `cut`: `-z` for both input and output

- - -

**Start `tmux` with programs in windows**

    #!/bin/bash

    SESS=$$

    CMD[1]='top'
    CMD[2]='watch -n 1 cat /proc/mdstat'
    CMD[3]='while sleep 5; do date | tee -a ps; ps aux | tee -a ps; done'
        
    tmux new-session -s $SESS -n window0 -d bash

    for N in $(seq ${#CMD[*]}); do
        echo "Running command $N: ${CMD[$N]}"
        tmux new-window -t $SESS:$N -n windows$N "${CMD[$N]}"
    done
    echo "Done. press enter to attach tmux..."
    read

    tmux select-window -t $SESS:0
    tmux attach

- - -

**Window reorganization**

* `vim`: `C-W H/J/K/L`
* `tmux`: `C-B space`

- - -

**Online markdown editor**

<https://stackedit.io/editor>

- - -

**Copy from buffer in `tmux`**

<https://awhan.wordpress.com/2010/06/20/copy-paste-in-tmux/>

- - -

**`find` excluding some directories**

Find and print all files in `/` except those in directory `.git`:

    find / -type f \( -path './.git/*' -prune -o -print \)

Find and print all files in `cwd` except those in directory `.git` and those matching filename `.meta.*`:

    find . -type f \( \( -path './.git/*' -o -name '.meta.*' \) -prune -o -print \)

- - -

**JSON pretty-printing one-liner**

For an existing file:

    perl -MJSON::PP -MFile::Slurp -e '$x = JSON::PP->new->utf8->pretty; print $x->encode($x->decode(read_file($ARGV[0])));' file.json

For an input from another command:

    command | perl -MJSON::PP -e '$x = JSON::PP->new->ascii->pretty; print $x->encode($x->decode(<>));'

For multiple JSONs (one per line) as an input from another command:

    command | perl -MJSON::PP -ne '$x = JSON::PP->new->ascii->pretty; print $x->encode($x->decode($_));'

- - -

**Wireshark for remote machine**

    ssh root@$REMOTE tcpdump -U -i $IFACE $PCAPFILTER -w /dev/stdout | wireshark -k -i - -d $DISPLAYFILTER
    ssh root@$REMOTE tcpdump -U -i $IFACE $PCAPFILTER -w /dev/stdout | tshark -x -r - -O $VERBOSEPROTOS $CAPTUREFILTER

Examples:

    ssh root@ares tcpdump -U -i vlan2 host lj2b -w /dev/stdout | wireshark -k -i - -d 'icmp'
    ssh root@ares tcpdump -U -i vlan48 host aisa -w /dev/stdout | tshark -x -r - -O dns 'udp.port == 53'

- - -

**X11 forwarding through `ssh` after running `su`**

    client$ ssh -X user1@host2
    user1@host$ xauth list $DISPLAY
    user1@host$ su - user2
    user2@host$ xauth add <relevant_line_from_xauth_list>

Source: <https://debian-administration.org/article/494/Getting_X11_forwarding_through_ssh_working_after_running_su>

- - -

**`pstree`**

    pstree -aclnp

* `-a`: Show command line arguments
* `-c`: Disable compaction of identical subtrees
* `-l`: Display long lines
* `-n`: Sort processes with the same ancestor by PID instead of by name
* `-p`: Show PIDs

- - -

**Sizes of deleted files**

    lsof -Fns 2>/dev/null | grep ' (deleted)$' -B1 | grep '^s' -A1 | grep -v -- -- | cut -c 2- | paste - - | sort -n

Useful for overfilling volumes where `du` doesn't pinpoint the culprit.

Source of inspiration: <https://access.redhat.com/solutions/2316>

- - -

**Formatting `pcap` data with `tshark` for further processing**

    tshark -r $PCAP_FILE -Y $DISPLAY_FILTER -T fields -E header=y -E separator=/t -e $ATTR1 -e $ATTR2 ...

Example:

    tshark -r arp.pcap -Y arp -T fields -E header=y -E separator=/t \
        -e frame.number -e frame.time_epoch -e frame.time \
        -e arp.opcode -e arp.src.proto_ipv4 -e arp.dst.proto_ipv4

- - -

**Restore raw keyboard mode after `SysRq + R`**

    sudo kbd_mode -s -C /dev/tty7

Source: <https://unix.stackexchange.com/questions/19296/recovering-from-sys-rq-r>

- - -

**List available Compose key sequences**

    cat /usr/share/X11/locale/$LANG/Compose

- - -

**Packet flow in Netfilter**

- image: <https://upload.wikimedia.org/wikipedia/commons/3/37/Netfilter-packet-flow.svg>
- wikipage: <https://commons.wikimedia.org/wiki/File:Netfilter-packet-flow.svg>

**Client DUID sniffing**

    ssh root@$HOST tcpdump -U -i $IFACE udp dst port 547 -w /dev/stdout \
        | tshark -r - -T fields -E header=y -E separator=/t \
        -e eth.src -e dhcpv6.client_fqdn -e ipv6.src -e dhcpv6.duid.bytes

- - -

**Where to search for periodic jobs**

    ls /etc/cron.*
    cat /etc/crontab
    cat /etc/anacrontab
    ls /var/spool/cron/
    systemctl list-timers --all

- - -

**Check for too old uncommitted git changes:**

This is the best way (prints the mtime and path) for uncommited changes
older than 180 days:

    git status --porcelain -z | cut -z -c 4- | find -files0-from - -maxdepth 0 -mtime +180 -printf '%TF %TT %p\n'

This is a worse version for RHEL7-based systems which support neither
`cut -z`, nor `find -files0-from`. But good luck in case of spaces in the
filenames:

    git status --porcelain | cut -c 4- | xargs -i find {} -maxdepth 0 -mtime +180 -printf '%TF %TT %p\n'

- - -

**Package/file "DB" search for RHEL-based distros:**

    # list files provided by $PACKAGE_NAME
    dnf repoquery -l $PACKAGE_NAME
    # list packages providing $FILE_PATH
    dnf whatprovides $FILE_PATH

If the package/file is installed/present, using `rpm` is much faster:

    rpm -ql $PACKAGE_NAME
    rpm -qf $FILE_PATH

- - -

**One-step Debian upgrade:**

    apt -U full-upgrade --autoremove

- - -

**Other cheat sheets**

* Markdown: <http://packetlife.net/media/library/16/Markdown.pdf>
* Unix Toolbox: <http://cb.vu/unixtoolbox.xhtml>
* `vimdiff`: <https://gist.github.com/mattratleph/4026987>

- - -

**Notable articles**

* In defence of swap: <https://chrisdown.name/2018/01/02/in-defence-of-swap.html>

- - -

**Quick network interface rate-limiting**

    tc qdisc replace dev eth0 root tbf rate 1gbit burst 1g latency 1s
